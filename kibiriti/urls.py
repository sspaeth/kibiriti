# kibriti - backend for fire fighter alarm management
# Copyright (C) 2021 Sebastian Spaeth
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.contrib import admin
from django.urls import include, path
from . import views
from . import signals  # Load all signals that are being connected

urlpatterns = [
    path('', views.index, name='index'),
    path('alarm/', views.AlarmIndexView.as_view(), name="alarm-list"),
    path('alarm/<int:pk>/', views.AlarmDetailView.as_view(), name="alarm-detail"),
    path('alarm/last/', views.AlarmDetailView.as_view(), name="alarm-detail-last"),
    path('alarm/new/', views.AlarmCreateView.as_view(), name="alarm-create"),
    path('admin/', admin.site.urls),
    # accounts/* links (https://docs.djangoproject.com/en/2.2/topics/auth/default/#module-django.contrib.auth.views)
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/profile/', views.AccountProfileView.as_view(),
         name="accounts-profile"),
]
