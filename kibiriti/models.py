# kibriti - backend for fire fighter alarm management
# Copyright (C) 2021 Sebastian Spaeth
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.db import models
from django.contrib.auth.models import AbstractUser
from django.urls import reverse
from django.utils import timezone

class Address(models.Model):
    address = models.CharField(max_length=200)
    housenumber = models.CharField(max_length=20)
    lat = models.FloatField(null=True, blank=True)
    lon = models.FloatField(null=True, blank=True)

    def __str__(self):
        return "%s %s" % (self.address, self.housenumber)

class AlarmType(models.Model):
    short = models.CharField(max_length=20, unique=True,
                             help_text="Alarm type in short form.")
    long = models.CharField(max_length=200, help_text="The type of "\
           "alarm in a human readable form.")

    def __str__(self):
        return self.short

class Alarm(models.Model):
    """A Firealarm"""
    # no will be populated on form validation when left empty
    # TODO, make sure on saving, that (time__year, no) are unique
    no = models.PositiveIntegerField("Alarm no. in any given year",
                                     blank=True)
    time = models.DateTimeField('Start of Alarm', default=timezone.now)
    type =  models.ForeignKey(AlarmType, null=True, blank=True,
                              on_delete=models.CASCADE)
    address = models.ForeignKey(Address, null=True, blank=True,
                                on_delete=models.CASCADE)
    endtime = models.DateTimeField('End of Alarm', null=True, blank=True,
                                   default=None)
    members = models.ManyToManyField("Person", through="Participation",through_fields=('alarm','person'),)

    def num_participants(self):
        """Returns (Yes, no, unknown) participants"""
        participations = Participation.objects.filter(alarm=self)
        yes_num = participations.filter(yesno=True).count()
        no_num  = participations.filter(yesno=False).count()
        null_num= participations.filter(yesno=None).count()
        return (yes_num, no_num, null_num)

    def clean(self):
        """Invoked on form validation

        Add in a unique alarm no for that year
        """
        if self.no is None:
            # If no 'no' was given, use last no + 1 or 1
            al = Alarm.objects.filter(
                 time__year=self.time.year).order_by('no').last()
            self.no = 1
            if al is not None:
                self.no += al.no

    def __str__(self):
        return '%s (%d-%d)' % (self.type, self.time.year%100, self.no)

    def get_absolute_url(self):
        return reverse("alarm-detail", args=[self.id])

    class Meta:
        ordering = ["-time__year", "-no"]
        get_latest_by = ['time']


class RoleType(models.Model):
    """ Gruppenführer, AGT,..."""
    name = models.CharField(max_length=200, unique=True)
    desc = models.CharField(max_length=250, blank=True)
    logo = models.ImageField(upload_to="uploads/img", blank=True, null=True)

    def __str__(self):
        return self.name


class Participation(models.Model):
    person = models.ForeignKey("Person", on_delete=models.CASCADE)
    alarm = models.ForeignKey(Alarm, on_delete=models.CASCADE)
    # yesno: whether a person intends to participate or not
    #        null = unknown participation, e.g. no answer
    yesno = models.BooleanField(default=None, null=True)

    def __str__(self):
        part = "?"
        if self.yesno:
            part = "✓"
        if self.yesno == False:
            part = "💩"
        return f"{self.person.username} {part}"

    def __repr__(self):
        return f"Participation: {self.alarm} {self}"

    class Meta:
        ordering = ["alarm", "-yesno", "person"]

class Person(AbstractUser):
    """Provides additional data for a User"""
    # 'roles' is a set of RoleTypes that this Person has
    roles = models.ManyToManyField(RoleType, blank=True)
