# kibriti - backend for fire fighter alarm management
# Copyright (C) 2021 Sebastian Spaeth
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render
from django.views import generic

from .models import Alarm, Participation

class AlarmIndexView(LoginRequiredMixin, generic.ListView):
    """A view of all alarms"""
    model = Alarm
    context_object_name = 'alarms'

class AlarmDetailView(LoginRequiredMixin, generic.DetailView):
    """A view of a single alarm"""
    model = Alarm

    def get_object(self, **kwargs):
        try:
            self.kwargs['pk']
        except:
            return self.model.objects.latest()
        return super().get_object(**kwargs)

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # add in Participation data
        context['participation'] = Participation.objects.filter(alarm__id=context['object'].id)
        return context

class AlarmCreateView(PermissionRequiredMixin, generic.CreateView):
    permission_required = 'kibiriti.add_alarm'
    model  = Alarm
    fields = ['time','type','address','endtime'] #'__all__'

def index(request):
    return HttpResponse('Check out all <a href="/alarm/">alarms</a>.')

class AccountProfileView(LoginRequiredMixin, generic.TemplateView):
    template_name="kibiriti/accounts_profile.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # hardcode currently logged in user for now
        # TODO: enable passing in different user than current one, if caller is admin
        context['profile'] = self.request.user
        return context
