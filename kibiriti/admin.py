# kibriti - backend for fire fighter alarm management
# Copyright (C) 2021 Sebastian Spaeth
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from django.contrib import admin
from . import models

class PersonAdmin(admin.ModelAdmin):
    pass
admin.site.register(models.Person, PersonAdmin)

class AlarmAdmin(admin.ModelAdmin):
    pass
admin.site.register(models.Alarm, AlarmAdmin)

class AlarmTypeAdmin(admin.ModelAdmin):
    pass
admin.site.register(models.AlarmType, AlarmTypeAdmin)

class AddressAdmin(admin.ModelAdmin):
    pass
admin.site.register(models.Address, AddressAdmin)

class ParticipationAdmin(admin.ModelAdmin):
    pass
admin.site.register(models.Participation, ParticipationAdmin)

class RoleTypeAdmin(admin.ModelAdmin):
    pass
admin.site.register(models.RoleType, RoleTypeAdmin)
